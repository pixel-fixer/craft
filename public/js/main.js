var widgetTimeout;

$(document).ready(function(){
    updateCart();
    vkWidget();

    $('select').material_select();

    $('.fancybox').fancybox();

    /*
    $.ajax({
        url: "/catalog_menu",
        success: function(data){
            $('.top_catalog_links').append(data);
        }
    });
    */

	$(window).resize(function(){
		clearTimeout(widgetTimeout);

		widgetTimeout = setTimeout(function(){
			vkWidget();
		}, 500);

	});

    // Верхнее меню
    $(document)
    .on('mouseover', 'a.main_link[data-formenu]', function(ev){
        $('a.main_link[data-formenu]').removeClass('active');
        $('.catalog__menu__sub').css('display', 'none');
        var $this = $(this).addClass('active');

        $('.overlay').stop(true).fadeIn(400);
        $('.catalog__menu__sub[data-menu="' + $this.attr('data-formenu') + '"]').css('display', 'block');
    })
    .on('mouseleave', 'a.main_link[data-formenu]', function(ev){
        var leave_to = $(document.elementFromPoint(ev.pageX, ev.pageY));

        if( !leave_to.is('.catalog__menu__sub') ){
            $(this).removeClass('active');
            $('.catalog__menu__sub').css('display', 'none');

            if( !leave_to.is('a.main_link[data-formenu]') )
                $('.overlay').css('display', 'none');
        }

    })
    .on('mouseleave', '.catalog__menu__sub', function(ev){
        var leave_to = $(document.elementFromPoint(ev.pageX, ev.pageY));

        if( !leave_to.is('a.main_link[data-formenu]') ){
            $('a.main_link[data-formenu]').removeClass('active');
            $('.overlay, .catalog__menu__sub').css('display', 'none');
        }
    });

    $('select[name=category_id]').change(function(){
        var category_id = $(this).val();
        $.get('/ajax/get_category_property/'+category_id, {}, function(response){
            $('#LoadProperty').html(response);
        })
    });

    var loadProperty = $('.jsLoadProperty').val();
    if (loadProperty != ''){
        var category_id = $('select[name=category_id]').val();
        /*
        $.get('/ajax/get_category_property/'+category_id+'?product_id='+loadProperty, {}, function(response){
            $('#LoadProperty').html(response);
        });
        */
    }



    function updateQuantity(id, quantity, token) {
        $.post('/cart/update_quantity', {id: id, quantity: quantity, _token: token}, function (res) {
            location.reload();
        });
    }

    $('.cart__list__quantity input').change(function () {
        var val = parseInt($(this).val());
        val = val > 0 ? val : 1;
        $(this).val(val);
        updateQuantity($(this).data().id, val, $(this).data().token);
     });

    $('.cart__list__quantity__up').click(function (e) {
        var $input = $(this).prev();
        $input.val( parseInt($input.val()) + 1).change();
        e.preventDefault();
        return false;
    });

    $('.cart__list__quantity__down').click(function (e) {
        var $input = $(this).next();
        $input.val( $input.val() - 1).change();
        e.preventDefault();
        return false;
    });
});

if($(this).scrollTop() > 120){
    if(!$('#ajax_cart').is(':visible'))
        $('#ajax_cart').show('fast');
}else{
    $('#ajax_cart').hide('fast');
}

$(window).scroll(function () {
    if($(this).scrollTop() > 120){
        if(!$('#ajax_cart').is(':visible'))
            $('#ajax_cart').show('fast');
    }else{
        $('#ajax_cart').hide('fast');
    }

});

function addToCart(id, el) {
    if (!$(el).hasClass('in-cart')) {
        $.get('/cart/add/'+ id, {id: id}, function (res) {
            $(el).addClass('in-cart');
            Materialize.toast('Товар добавлен в корзину!', 2000)
            updateCart();
        });
    }
}

function updateCart() {
    $.post('/cart/quantity', function (res) {
        $('#cart').attr('data-quantity', (res.quantity == '0' ? '' : res.quantity) );
   }, 'json');
}

function vkWidget() {
	$('#vk_groups').html('').removeAttr('style');

	if(window.VK != undefined)
		VK.Widgets.Group("vk_groups", { mode: 3, width: 'auto' }, 141446563);

}
