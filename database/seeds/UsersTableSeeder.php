<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('users')->insert([
			'first_name' => 'Администратор',
			'last_name' => 'Общий аккаунт',
			'email' => 'admin@admin.com',
			'password' => bcrypt('admin'),
			'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
	    ]);

		DB::table('users')->insert([
			'first_name' => 'Александр',
			'last_name' => 'Пинашин',
			'email' => 'alex-p.s@yandex.ru',
			'password' => bcrypt('alex'),
			'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
	    ]);

		DB::table('users')->insert([
			'first_name' => 'Владимир',
			'last_name' => 'Ивлев',
			'email' => 'ivlev645@gmail.com',
			'password' => bcrypt('ivlev'),
			'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
	    ]);
    }
}
