<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
		DB::table('products')->insert([
			'user_id' => 1,
			'name'    => 'Тестовый товар',
			'code'    => 'one_product',
			'price'   => 1000,
            'category_id' => 2,
			'type'    => 'completed',
			'description' => 'Описание тестового товара с типом "Готовый товар", владелец товара admin. Конец описания',
			'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
		]);
		DB::table('products')->insert([
			'user_id' => 1,
			'name'    => 'Тестовый товар',
			'code'    => 'one_product10',
			'price'   => 1000,
            'category_id' => 2,
			'type'    => 'completed',
			'description' => 'Описание тестового товара с типом "Готовый товар", владелец товара admin. Конец описания',
			'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
		]);
		DB::table('products')->insert([
			'user_id' => 1,
			'name'    => 'Тестовый товар',
			'code'    => 'one_product1',
			'price'   => 1000,
            'category_id' => 2,
			'type'    => 'completed',
			'description' => 'Описание тестового товара с типом "Готовый товар", владелец товара admin. Конец описания',
			'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
		]);
		DB::table('products')->insert([
			'user_id' => 2,
			'name'    => 'Тестовый товар',
			'code'    => 'one_product2',
			'price'   => 1000,
            'category_id' => 2,
			'type'    => 'completed',
			'description' => 'Описание тестового товара с типом "Готовый товар", владелец товара admin. Конец описания',
			'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
		]);
		DB::table('products')->insert([
			'user_id' => 1,
			'name'    => 'Тестовый товар',
			'code'    => 'one_product3',
			'price'   => 1000,
            'category_id' => 2,
			'type'    => 'completed',
			'description' => 'Описание тестового товара с типом "Готовый товар", владелец товара admin. Конец описания',
			'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
		]);


	}
}