<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
		$categories = [
            'acsessuaru' => [ 'Аксессуары',
                [
                    'koshelki-i-vizitnitsi' => 'Кошельки и визитницы',
                    'sharfi-i-platki'       => 'Шарфы и платки',
                    'golovnie-ybori'        => 'Головные уборы',
                    'noski-i-chulki'        => 'Носки и чулки',
                    'naruchnie-chasi'       => 'Наручные часы',
                    'varezhki-i-perchatki'  => 'Варежки и перчатки',
                    'breloki'               => 'Брелоки',
                ]
            ],
			'tovary-dlya-doma'    => 'Товары для дома',
			'tovary-dlya-detey'    => 'Товары для детей',
			'igrushki-i-kukly'    => 'Игрушки и куклы',
			'kosmetika'    => 'Косметика',
			'zhenskya-odezhda-obuv'    => 'Женская одежда и обувь',
			'sumki'    => 'Сумки',
			'ykrashenija'           => [ 'Украшения',
				[
					 'brasleti'                 => [ 'Браслеты',
						[
							'brasleti-na-nogi'      => 'Браслеты на ноги',
							'naruchnie-brasleti'    => 'Наручные браслеты',
						]
					 ],
					 'sergi'                    => 'Серьги',
					 'koltsa'                   => 'Кольца',
					 'ykrashenija-na-sheu'      => 'Украшения на шею',
					 'broshki'                  => 'Броши',
					 'znachki'                  => 'Значки',
					 'komplekti-ykrashenii'     => 'Комплекты украшений',
					 'ukrashenija-dlja-volos'   => 'Украшения для волос',
					 'myshkie-ykrashenia'       => 'Мужские украшения',
					 'chetki'                   => 'Четки'
				]
			],

		];

		$this->insertArray($categories);
		App\Category::recount_tree();
	}

	private function insertArray($categories, $level = 0, $parent = 0, $counter = 0){
		$level++;
		foreach ($categories as $code => $category){
			$counter++; // Счётчик итераций т.к. insert() не возвращает вставленный ID а он нам нужен для указания родителя
			$catName = is_array($category) ? $category[0] : $category;

			DB::table('categories')->insert([
				'level'             => $level,
				'parent_id'         => $parent,
				'name'    => $catName,
				'code'     => $code
			]);

			if(is_array($category))
				$counter = $this->insertArray($category[1], $level, $counter, $counter);
		}
		return $counter;
	}

}
