<?php

use Illuminate\Database\Seeder;

class PropertyEnumTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('property_enum')->insert([
            'property_id'    => 1,
            'name'    => 'Из Кожи',
            'code'    => 'is_kozhi',
            'sort'   => 1,
            'active' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('property_enum')->insert([
            'property_id'    => 1,
            'name'    => 'Из дерева',
            'code'    => 'is_dereva',
            'sort'   => 1,
            'active' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('property_enum')->insert([
            'property_id'    => 1,
            'name'    => 'Из меха',
            'code'    => 'is_mecha',
            'sort'   => 1,
            'active' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

    }
}