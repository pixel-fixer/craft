<?php

use Illuminate\Database\Seeder;

class PropertyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('property')->insert([
            'name'    => 'Материал',
            'code'    => 'material',
            'sort'   => 1,
            'active' => 1,
            'type'    => 'list',
            'description' => 'Описание тестового товара с типом "Готовый товар", владелец товара admin. Конец описания',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

    }
}