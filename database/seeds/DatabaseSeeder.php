<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Model::unguard();

		// Пользователи и права доступа
		$this->call(UsersTableSeeder::class);
		$this->call(RolesTableSeeder::class);
		$this->call(RoleUserTableSeeder::class);
		$this->call(PermissionsTableSeeder::class);
		$this->call(PermRoleTableSeeder::class);

		// Товары
	    $this->call(ProductsTableSeeder::class);
	    $this->call(CategoriesTableSeeder::class);
	    $this->call(CatProductTableSeeder::class);
	    $this->call(PropertyTableSeeder::class);
	    $this->call(PropertyEnumTableSeeder::class);
	    $this->call(PropertyCategoryTableSeeder::class);


		Model::reguard();
    }
}
