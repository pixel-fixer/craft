<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
		DB::table('roles')->insert([
			'role_title' => 'Администратор',
			'role_name' => 'admin',
		]);

		DB::table('roles')->insert([
			'role_title' => 'Модератор',
			'role_name' => 'moder',
		]);
	}
}
