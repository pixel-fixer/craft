<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
		DB::table('permissions')->insert([
			'permission_title' => 'Доступ в панель управления',
			'permission_name' => 'control_view'
		]);
	}
}
