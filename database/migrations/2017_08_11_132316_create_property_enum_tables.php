<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyEnumTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_enum', function (Blueprint $table) {
            $table->Increments('id');
            $table->integer('property_id');
            #$table->foreign('property_id')->references('id')->on('property')->onDelete('cascade');;
            $table->string('name', 255);
            $table->string('code', 255);
            $table->integer('sort')->default('1');
            $table->integer('active')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('property_enum');
    }
}
