<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::create('products', function (Blueprint $table) {
			$table->mediumIncrements('id');
			$table->mediumInteger('user_id')->unsigned();
			$table->string('name', 255);
			$table->string('code', 255)->unique();
			$table->integer('active')->default('1');
			$table->integer('category_id');
			$table->decimal('price', 8, 2);
			$table->enum('type', ['completed', 'to_order', 'example']);
			$table->text('description');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
	}
}
