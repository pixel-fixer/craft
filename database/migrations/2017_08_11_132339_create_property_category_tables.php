<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyCategoryTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_category', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->integer('property_id');
            #$table->foreign('property_id')->references('id')->on('property');
            $table->integer('category_id');
            #$table->foreign('category_id')->references('id')->on('categories');
            $table->enum('show_filter', ['Y', 'N']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('property_category');
    }
}
