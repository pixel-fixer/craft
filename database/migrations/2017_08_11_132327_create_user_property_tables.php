<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPropertyTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_property', function (Blueprint $table) {
            $table->Increments('id');
            $table->integer('property_id');
            #$table->foreign('property_id')->references('id')->on('property');
            $table->integer('product_id');
            #$table->foreign('product_id')->references('id')->on('product');
            $table->integer('enum_id');
            #$table->foreign('enum_id')->references('id')->on('property_enum');
            $table->string('value', 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_property');
    }
}
