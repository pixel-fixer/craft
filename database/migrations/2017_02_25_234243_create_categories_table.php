<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::create('categories', function (Blueprint $table) {
			$table->smallIncrements('id');
			$table->smallInteger('left')->unsigned()->default(0);
			$table->smallInteger('right')->unsigned()->default(0);
			$table->tinyInteger('level')->unsigned()->default(0);
			$table->smallInteger('parent_id')->unsigned()->default(0);
			$table->string('name', 255);
			$table->string('code');
			$table->string('meta_title');
			$table->string('meta_keywords');
			$table->string('meta_description');
			$table->text('small_seo_text');
			$table->text('big_seo_text');
		});

		$sql = <<<SQL
			DROP PROCEDURE IF EXISTS `recount_tree`;
		    
			CREATE PROCEDURE `recount_tree`(IN PID INTEGER, INOUT IRIGHT INTEGER, IN LEVELS INTEGER)
			BEGIN
				
				DECLARE I INT DEFAULT 0;
				DECLARE L INT DEFAULT 0;
				DECLARE R INT DEFAULT 0;
				DECLARE N VARCHAR(255);
				DECLARE done INTEGER DEFAULT 0;
				
				DECLARE parentID CURSOR FOR SELECT `id` FROM `categories` WHERE `parent_id` = PID;
				DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done=1;
				
				-- SET max_sp_recursion_depth=255;
				
				SET LEVELS = LEVELS +1;
				
				OPEN parentID;
				WHILE done = 0 DO 
						FETCH parentID INTO I;
						IF NOT done THEN
						
							IF i > 0 THEN
								SET L = IRIGHT ;
								SET IRIGHT = IRIGHT + 1;
								CALL recount_tree(I, IRIGHT, LEVELS);
								SELECT I, L, IRIGHT, LEVELS;
								
								PREPARE update_one FROM 'update `categories` SET `left` = ?, `right` = ?, `level` = ? where `id` = ?';
								SET @l = L;
								SET @r = IRIGHT;
								SET @i = I;
								SET @lev = LEVELS;
								EXECUTE update_one USING @l, @r,  @lev, @i;
								SET IRIGHT = IRIGHT+1;
							END IF;
						END IF;
				END WHILE;
					
				CLOSE parentID;
			
			END;
SQL;
		DB::connection()->getPdo()->exec($sql);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		$sql = "
			DROP PROCEDURE IF EXISTS `recount_tree`;
		";
		DB::connection()->getPdo()->exec($sql);
		Schema::drop('categories');
	}
}
