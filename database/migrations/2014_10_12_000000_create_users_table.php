<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::create('users', function (Blueprint $table) {
			$table->mediumIncrements('id');
			$table->string('first_name', 100);
			$table->string('last_name', 100);
			$table->string('password', 60);
			$table->string('photo', 60);
			$table->enum('sex', ['men', 'women']);
			$table->text('about');
			$table->string('email', 100)->unique();
			$table->rememberToken();
			$table->timestamps();
			$table->softDeletes();
		});
	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
