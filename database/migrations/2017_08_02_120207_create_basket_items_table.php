<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBasketItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('basket_items', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->mediumInteger('user_id')->unsigned();
            $table->mediumInteger('order_id');
            $table->string('name', 255);
            $table->string('code', 255);
            $table->integer('category_id');
            $table->decimal('price', 8, 2);
            $table->enum('type', ['completed', 'to_order', 'example']);
            $table->text('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('basket_items');
    }
}
