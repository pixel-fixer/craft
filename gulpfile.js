var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.stylus('main.styl');

    mix.styles([
        '../bower_components/normalize-css/normalize.css'
    ],
    'public/css/vendor.css');

    mix.scripts([
        //'../../assets/bower_components/jquery/dist/jquery.js',
        'main.js'
    ],
    'public/js/vendor.js');
});
