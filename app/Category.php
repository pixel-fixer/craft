<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	protected $table = 'categories';

	protected $guarded = ['left', 'right', 'level'];

    public function properties()
    {
        return $this->hasMany('App\PropertyCategory', 'category_id', 'id');
    }

	public static function recount_tree(){
		$sql = "
			SET max_sp_recursion_depth=255;
			SET @val = 0;
			SET @level = 0;
			CALL recount_tree(0, @val, @level);
		";
		\DB::connection()->getPdo()->exec($sql);
	}
}