<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\CartController;

class Product extends Model
{
    protected $table = 'products';

    public function images()
    {
        return $this->hasMany('App\ImageTable', 'item_id', 'id');
    }

    public function properties()
    {
        return $this->hasMany('App\UserProperty', 'product_id', 'id');
    }

    public function inCart(){
        $cart = new CartController();
        return $cart->has($this->id);
    }

}