<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
	use Authenticatable, CanResetPassword, SoftDeletes;

	protected $table = 'users';

	protected $fillable = ['first_name', 'last_name', 'username', 'email', 'password'];

	protected $hidden = ['password', 'remember_token'];

	/*
	|--------------------------------------------------------------------------
	| ACL методы
	|--------------------------------------------------------------------------
	*/

	/**
	 * Checks a Permission
	 *
	 * @param  String permission Name of a permission (i.e: manage_user)
	 * @return Boolean true if has permission, otherwise false
	 */
	public function can($permission = null)
	{
		return !is_null($permission) && $this->checkPermission($permission);
	}

	/**
	 * Check if the permission matches with any permission user has
	 *
	 * @param  String permission name of a permission
	 * @return Boolean true if permission exists, otherwise false
	 */
	protected function checkPermission($perm)
	{
		$permissions = $this->getAllPernissionsFormAllRoles();

		$permissionArray = is_array($perm) ? $perm : [$perm];

		return count(array_intersect($permissions, $permissionArray));
	}

	/**
	 * Get all permission names from all permissions of all roles
	 *
	 * @return Array of permission names
	 */
	protected function getAllPernissionsFormAllRoles()
	{
		$permissionsArray = [];

		$permissions = $this->roles->load('permissions')->fetch('permissions')->toArray();

		return array_map('strtolower', array_unique(array_flatten(array_map(function ($permission) {

			return array_fetch($permission, 'permission_name');

		}, $permissions))));
	}

	/*
	|--------------------------------------------------------------------------
	| Relationship Methods
	|--------------------------------------------------------------------------
	*/

	/**
	 * Many-To-Many Relationship Method for accessing the User->roles
	 *
	 * @return QueryBuilder Object
	 */
	public function roles()
	{
		return $this->belongsToMany('App\Role');
	}
}