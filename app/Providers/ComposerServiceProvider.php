<?php

namespace App\Providers;
use App\Category;
use App\Http\Controllers\CartController;
use App\Product;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        // Using Closure based composers...
        View::composer('layouts.main', function ($view) {

            $cart = new CartController();
            $categories = Category::where('level', '=', 1)->get(['id','code','name']);
            $view->with('categories', $categories)
                ->with('cart_count',$cart->count());

        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
