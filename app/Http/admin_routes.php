<?php

Route::group(['middleware' => 'acl:control_view'], function () {
	// Статичные страницы
	Route::get('control', ['as' => 'home', 'uses' => 'Admin\StaticController@home']);

});
