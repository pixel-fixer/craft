<?php namespace App\Http\Middleware;

use Closure;

// Проверка прав доступа
class CheckPermission
{

	public function handle($request, Closure $next, $permission = null)
	{
		// Сразу проверяем авторизован или нет
		if (!app('Illuminate\Contracts\Auth\Guard')->guest()) {

			// Если есть доступ то пропускаем
			if ($request->user()->can($permission)) {

				return $next($request);
			}
		}

		return $request->ajax ? response('Unauthorized.', 401) : redirect('/login');
	}
}