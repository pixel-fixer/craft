<?php
require_once 'admin_routes.php';

// Статичные страницы
Route::get('/', ['as' => 'home', 'uses' => 'StaticController@home']);

// Контроллеры аккаунта
Route::get('personal', ['as' => 'personal_page', 'uses' => 'AccountController@getPersonal']);

Route::get('personal/catalog', ['as' => 'personal_catalog', 'uses' => 'CatalogController@viewCatalog']);
Route::get('personal/catalog/add', ['as' => 'personal_product_add_form', 'uses' => 'CatalogController@viewAddNewProduct']);
Route::post('personal/catalog/add', ['as' => 'personal_product_add', 'uses' => 'CatalogController@AddNewProduct']);
Route::get('personal/catalog/edit/{id}', ['as' => 'personal_product_edit_form', 'uses' => 'CatalogController@viewEditProduct']);
Route::post('personal/catalog/edit/{id}', ['as' => 'personal_product_edit', 'uses' => 'CatalogController@AddNewProduct']);

Route::post('personal', ['as' => 'personal_update', 'uses' => 'AccountController@updatePersonal']);

// Контроллеры каталога
Route::get('catalog', ['as' => 'catalog', 'uses' => 'CatalogController@viewIndex']);
Route::get('catalog_menu', ['as' => 'catalog_menu', 'uses' => 'CatalogController@viewTopMenu']);
Route::get('catalog/{category}', ['as' => 'catalog_category', 'uses' => 'CatalogController@viewCategory']);
Route::get('product/{product}', ['as' => 'catalog_product', 'uses' => 'CatalogController@viewProduct']);
Route::get('ajax/get_category_property/{category}', ['as' => 'get_category_property', 'uses' => 'CatalogController@viewCategoryProperty']);


// Маршруты аутентификации...
Route::get('login', ['as' => 'login_page', 'uses' => 'Auth\AuthController@getLogin']);
Route::post('login', ['as' => 'login', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@getLogout']);

// Маршруты регистрации...
Route::get('register', ['as' => 'register_page', 'uses' => 'Auth\AuthController@getRegister']);
Route::post('register', ['as' => 'register', 'uses' => 'Auth\AuthController@postRegister']);

// Маршруты запроса ссылки для сброса пароля...
Route::get('password/email', ['as' => 'get_reset_token_page', 'uses' => 'Auth\PasswordController@getEmail']);
Route::post('password/email', ['as' => 'get_reset_token', 'uses' => 'Auth\PasswordController@postEmail']);

// Маршруты сброса пароля...
Route::get('password/reset/{token}', ['as' => 'reset_password_page', 'uses' => 'Auth\PasswordController@getReset']);
Route::post('password/reset', ['as' => 'reset_password', 'uses' => 'Auth\PasswordController@postReset']);

// Корзина
Route::get('cart', ['as' => 'view_cart', 'uses' => 'CartController@show']);
Route::get('cart/add/{id}', 'CartController@add');
Route::get('cart/delete/{id}', 'CartController@delete');
Route::post('cart/quantity', 'CartController@getQuantity');
Route::post('cart/update_quantity', 'CartController@updateQuantity');
