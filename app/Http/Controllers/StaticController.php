<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
//use Illuminate\Http\Request;

//use App\Http\Requests;
//use App\Http\Controllers\Controller;

class StaticController extends Controller
{

	public function home(){
        $categories = Category::where('level', '=', 1)->take(7)->get(['id','code','name']);

        $products = Product::Where('products.active', '=', '1')->take(7)->get();

		return view('home')
			->with('title', 'авторские изделия ручной работы, хенд мейд')
			->with('categories', $categories)
			->with('products', $products);
	}

}
