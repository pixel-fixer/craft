<?php

namespace App\Http\Controllers;

use DB;
use App\Category;
use App\Product;
use App\User;
use App\ImageTable;
use App\UserProperty;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Transliterate;
use Intervention\Image\ImageManagerStatic as Image;

class CatalogController extends Controller
{

	public function viewIndex(){
		$categories = Category::where('level', '=', 1)->get(['id','code','name']);


		return view('catalog.index')
			->with('title', 'Каталог товаров')
			->with('categoryFilter', [])
			->with('categories', $categories);
	}

	public function viewCategory($categoryCode){
		$category = Category::whereCode($categoryCode)->first();

		if(!$category)
			throw new HttpException(404);


		$categories = Category::whereParentId($category->id)->get(['code','name']);

		$filter_category = [];
		$filter_category[] = $category->id;

		foreach ($categories as $cat){
			$filter_category[] = $cat->id;
		}

		$filter = DB::select('SELECT 
			p.`id` as property_id,
			p.`name` as property_name, p.`code` as property_code,
			pe.`id`, pe.name, pe.code, pe.sort,
			(SELECT COUNT(id) FROM user_property AS up WHERE up.property_id = pe.`property_id` AND up.enum_id = pe.`id`) AS count_product
		FROM property_enum AS pe
		LEFT JOIN property AS p ON p.`id` = pe.`property_id`
		WHERE pe.`property_id` IN
			(
				SELECT pc.`property_id`
				FROM property_category AS pc
				WHERE pc.`category_id`IN (:category)  AND pc.`show_filter` = \'Y\'
			)
			AND pe.`active` = \'1\'
			ORDER BY pe.`property_id` ASC, pe.`sort` ASC ', ['category' => implode(',', $filter_category)]);


        $categoryFilter = [];
		foreach ($filter as $f){
			$categoryFilter[$f->property_id]['NAME'] = $f->property_name;
			$categoryFilter[$f->property_id]['VALUES'][] = $f;
		}
		$filter = isset($_GET['filter'])? $_GET['filter'] : [];

        return view('catalog.category')
			->with('title', $category->meta_title . ' | Каталог товаров')
			->with('description', $category->meta_description)
			->with('keywords', $category->meta_keywords)
			->with('categoryFilter', $categoryFilter)
			->with('category', $category)
			->with('categories', $categories)
			->with('products', $this->productList($category, $filter))
			->with('breadCrumbs', self::categoryBreadcrumbs($category));
	}


	public function viewTopMenu(){

		$list = self::getList(array(">=LEFT"=>"0", "<RIGHT"=>"100", "<LEVEL"=>2));

		return view('catalog.menu')
			->with('menu', $list);
	}

	/**
	 * one product card
	 * @param $product_code
	 * @return $this
	 */
	public function viewProduct($product_code){

		$product = Product::Where('code', '=', $product_code)->first();

		if(!$product)
			throw new HttpException(404);
		$category = Category::find($product->category_id)->first();

		$master = User::where('id' ,'=', $product->user_id)->first();

		# properties from product
		$results = DB::select('SELECT p.id, p.type AS p_type, p.name,
			IF (p.type = 2, up.value, pe.name) AS val
			FROM user_property AS up
			LEFT JOIN property AS p ON p.id = up.property_id
			LEFT JOIN property_enum AS pe ON pe.id = up.enum_id
			WHERE up.product_id = :id', ['id' => $product->id]);

		$properties = [];

		foreach ($results as $property){
			$properties[$property->id]['NAME'] = $property->name;
			$properties[$property->id]['VALUE'][] = $property->val;
		}


		return view('catalog.product')
			->with('title', $product->name . " SEO MASK")
			->with('breadCrumbs', self::categoryBreadcrumbs($category))
			->with('properties', $properties)
			->with('master', $master)
			->with('product', $product);
	}

	public function productList($category, $filter = []){
		$subCategory = self::getList([">=LEFT" => $category->left, "<=RIGHT" => $category->right]);

		$inCategory = [];
		foreach ($subCategory as $sub){ $inCategory[] = $sub->id; }

		$inProducts = [];
		if (count($filter)>0){
			$filterProduct = UserProperty::whereIn('enum_id', $filter)->get(['product_id']);
			foreach ($filterProduct as $fp){
				$inProducts[] = $fp->product_id;
			}
		}

		# отфильтруем товары в категории
		if (count($inProducts)){
			$products = Product::WhereIn('category_id', $inCategory)
				->Where('products.active', '=', '1')
				->whereIn('id', $inProducts)
				->paginate(8);
		}else{
			$products = Product::WhereIn('category_id', $inCategory)
				->Where('products.active', '=', '1')
				->paginate(8);
		}


		return $products;
	}

	public static function getList($filter = array()){

		$category = new Category();

		foreach($filter as $code => $value){
			preg_match_all("#^([<>=!]*)([A-Za-z_-]+)#", $code, $where);

			   $z = "=";
				switch ($where[1][0]){
					case "=": $z = "="; break;
					case "!": $z = "<>"; break;
					case "<": $z = "<"; break;
					case ">": $z = ">"; break;
					case ">=": $z = ">="; break;
					case "<=": $z = "<="; break;
				}
				$field = $where[2][0];
				$category = $category->where($field, $z, $value);
		}

		$category = $category->orderby("left", 'asc');

		return $category->get(['id', 'name', 'code', 'parent_id', 'level']);
	}

	public static function categoryBreadcrumbs(Category $category){
		$links = [];
		$links[] = [
			'url' => '/catalog',
			'anchor' => 'Каталог'
		];

		$cats = Category::where('left', '<', $category->right)
			->where('right', '>', $category->left)
			->where('level', '<=', $category->level)
			->get(['code','name']);

		foreach ($cats as $cat)
			$links[] = [
				'url'	=> route('catalog_category', ['category' => $cat->code]),
				'anchor' => $cat->name
			];

		return $links;
	}

	/**
	 * List product with users
	 * @return $this
	 */
	public function viewCatalog(){

		$personalProducts = Product::leftjoin('categories', 'products.category_id', '=', 'categories.id')
			->Where("user_id", '=', Auth::user()->id)
			->groupBy('products.id')
			->paginate(5,['products.*', 'categories.name as category_name']);

		return view('cabinet.catalog')
			->with('title', 'Персональный каталог')
			->with('products', $personalProducts);
	}

	/**
	 * Get all product auth user
	 * @return mixed
	 */
	public function getUsersProduct(){
		$products = Product::where('user_id', '=', Auth::user()->id)->get();

		return $products;
	}

	/**
	 * Add new product Form
	 * @return $this
	 */
	public function viewAddNewProduct(){

		$categories = Category::orderBy('left')->get();

		$categoryView = [];
		foreach ($categories as $category){
			$categoryView[$category->id] = $category->name;
		}

		return view('catalog.product.add')
			->with('title', 'Добавление нового товара')
			->with('category', $categoryView);
	}

	/** Edit product form
	 * @param $id
	 * @return $this
	 */
	public function viewEditProduct($id){

		$product = false;
		if ($id) {
			$product = Product::Where('products.id', '=', $id)
				->Where('products.user_id', '=', Auth::user()->id)
				->first();
		}

		if (!$product)
			throw new HttpException(404);

		$categories = Category::orderBy('left')->get();

		$categoryView = [];
		foreach ($categories as $category){
			$categoryView[$category->id] = $category->name;
		}

		return view('catalog.product.edit')
			->with('title', 'Добавление нового товара')
			->with('product', $product)
			->with('category', $categoryView);
	}

	/**
	 * Add and Edit products
	 * @param Request $request
	 * @return $this
	 */
	public function AddNewProduct(Request $request){

		$this->validate($request,[
			'name' => "required",
			'description' => 'max:500',
			'category_id' => "required"
		]);

		//dd($request->input('property'));

		if ($request->input('id')){
			$product = Product::find($request->input('id'));

			# check The goods belongs to the user
			if ($product->user_id != Auth::user()->id){
				$messages[] = 'Ошбика, нет прав на редактирваоние данного товара';
				return Redirect::route('personal_catalog')
					->withErrors($messages);
			}
		}else{
			$product = new Product();
		}

		if($request->has('name')){
			$product->name = $request->input('name');
			$product->code = Transliterate::make($product->name, ['type' => 'filename', 'lowercase' => true]);;
		}
		if($request->has('description')){
			$product->description = $request->input('description');
		}
		if($request->has('price')){
			$product->price = $request->input('price');
		}
		if($request->has('active')){
			$product->active = $request->input('active');
		}
		if($request->has('category_id')){
			$product->category_id = $request->input('category_id');
		}

		$product->user_id = Auth::user()->id;

		if ($product->save()) {

			# control images from product
			if ($product->id) {
				# add image for product
				if ($request->file('image')) {

					foreach($request->file('image') as $k => $image) {

						if (!empty($image)) {
							$filename = time() . "_" . $k . '.' . $image->getClientOriginalExtension();

							$path_small = public_path('img/products/small_' . $filename);
							$path_big = public_path('img/products/' . $filename);

							Image::make($image->getRealPath())->resize(200, 200)->save($path_small);
							Image::make($image->getRealPath())->resize(800, 800)->save($path_big);

							$image = new ImageTable();
							$image->item_id = $product->id;
							$image->type = 'product';
							$image->patch = $filename;
							$image->save();
						}
					}
				}

				# delete image from product
				if ($request->input('del_image')){
					foreach($request->input('del_image') as $del_image){
					   $image_delete = ImageTable::find($del_image);

					   # check The picture belongs to the goods
					   if ($image_delete->item_id == $product->id){
						   $image_delete->delete();
					   }
					}
				}

				# save property from product
				if ($request->has('property')){
					# удалим все предыдушие значения свойств данного товра
					UserProperty::where('product_id', '=', $product->id)->delete();

					# Добавим новые значения свойств
					foreach ($request->input('property') as $property_id => $property_value){
						foreach ($property_value as $value) {
							$userProperty = new UserProperty();
							$userProperty->product_id = $product->id;
							$userProperty->property_id = $property_id;
							$userProperty->enum_id = is_numeric($value) ? $value : 1;
							$userProperty->value = $value;
							$userProperty->save();
						}
					}
				}
			}

			$messages[] = 'Товар успешно добавлен';
		}

		if ($request->input('apply')){
			return Redirect::route('personal_catalog')
				->withErrors($messages);
		}

		if ($request->input('id')) {
			return Redirect::route('personal_product_edit', $product->id)
				->withInput()
				->withErrors($messages);
		}else{
			return Redirect::route('personal_product_add')
				->withInput()
				->withErrors($messages);
		}
	}

	public function viewCategoryProperty($id){
		$category = Category::find($id);

		$product_id = isset($_GET['product_id'])?$_GET['product_id']:false;
		$properties = [];

		if (!empty($product_id)){
			$product = Product::find($product_id);
			# совйтва подгружать будем только у товаров авторизованного пользователя и если он ему принадлежит
			if ($product->user_id == Auth::user()->id) {
				foreach ($product->properties as $property) {
					$properties[$property->property_id][$property->enum_id] = $property->value;
				}
			}
		}

		return view('catalog.property.edit')
			->with('properties', $properties)
			->with('category', $category);
	}
}
