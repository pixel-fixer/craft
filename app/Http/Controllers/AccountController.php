<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
//use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\Redirect;
use Intervention\Image\ImageManagerStatic as Image;

class AccountController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function getPersonal(){
		return view('auth.personal')
			->with('title', 'Личный кабинет');
	}

	public function updatePersonal(Request $request){
		$messages = [];

		// Пока без валидации, просто проверка на пустоту
		if($request->has('first_name')) {
			Auth::user()->first_name = $request->input('first_name');
		}
		if($request->has('last_name')) {
			Auth::user()->last_name = $request->input('last_name');
		}
		if($request->has('email')){
			Auth::user()->email = $request->input('email');
		}
		if($request->has('sex')){
			Auth::user()->sex = $request->input('sex');
		}
		if($request->has('about')){
			Auth::user()->about = $request->input('about');
		}

		// Если запрошена смена пароля
		if($request->has('password')){
			$pass = $request->input('password');
			$pass_repeat = $request->input('repeat_password');

			if($pass != $pass_repeat)
				$messages[] = 'Указанные пароли не совпадают';
			if(mb_strlen($pass, 'utf-8') < 3)
				$messages[] = 'Минимальная длина пароля - 3 символа';

			Auth::user()->password = bcrypt($pass);
		}

		if(count($messages) == 0){
		    #echo "<pre>"; print_r($request->file('photo')); echo "</pre>"; die();
            # save avatar user
            if ($image = $request->file('photo')) {
                if (!empty($image)) {
                    $filename = time() . '.' . $image->getClientOriginalExtension();

                    $path_small = public_path('img/personal/small_' . $filename);
                    $path_big = public_path('img/personal/' . $filename);

                    Image::make($image->getRealPath())->resize(100, 100)->save($path_small);
                    Image::make($image->getRealPath())->resize(500, 500)->save($path_big);

                    Auth::user()->photo = $filename;
                }

            }


			Auth::user()->save();
			$messages[] = 'Учётные данные успешно обновлены';
		}

		return Redirect::route('personal_page')
			->withErrors($messages);
	}



}
