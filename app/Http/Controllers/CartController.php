<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//
//use App\Http\Requests;
//use App\Http\Controllers\Controller;
//
use App\User;
use Illuminate\Session\SessionManager;

use Illuminate\Support\Collection;
use App\Product;
use Intervention\Image\Exception\NotFoundException;

class CartController extends Controller
{

    protected function getItems()
    {
        $items = session('cart');
        return $items instanceof Collection ? $items : new Collection;
    }

    protected function setItems($items)
    {
        return session(['cart' => $items]);
    }

    protected function getItem($id){
        return $this->getItems()->get($id);
    }

    protected function setItem($item){
        $items = $this->getItems();
        $items->put($item['id'], $item);
        $this->setItems($items);
    }

    public function has($id){
        $items = $this->getItems();
        return $items->has($id);
    }

    public function updateQuantity(Request $request){
        $id = $request->id;
        $quantity = $request->quantity;
        if(!$id && !$quantity){
            throw new NotFoundException();
        }
        $id = intval($id);
        $quantity = $quantity > 0 ? $quantity : 1;
        if($this->has($id)){
            $item = $this->getItem($id);
            $item['quantity'] = $quantity;
            $this->setItem($item);
            return response()->json(['STATUS'=> 'OK']);
        }else{
            //TODO throw error?
        }
    }


    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function add($id)
    {
        $id = intval($id);

        if ($this->has($id)) {
            $item = $this->getItem($id);
            $item['quantity'] = $item['quantity'] + 1;
        } else {
            $product = Product::find($id);
            if ($product) {
                $item = $product->toArray();
                $item['quantity'] = 1;
                $item['images'] = $product->images->toArray();
            } else {
                return response()->json(['STATUS' => 'ERROR', 'MESSAGE' => 'Product not found']);
            }
        }
        $this->setItem($item);
        return response()->json(['STATUS' => 'OK']);
    }

    public function delete($id){
        $item = $this->getItems();
        $item->pull(intval($id));
        //return response()->json($this->getItems());
        return redirect('cart');
    }

    public function show(){
        $items = $this->getItems();
        $items_grouped = $items->groupBy('user_id')->all();
        $sum = [];
        foreach ($items_grouped as $k => $i){
            $user = User::find($k);
            if($user){
                $sum[$k]['id'] = $user->id;
                $sum[$k]['name'] = $user->first_name.' '.$user->last_name;
            }
            $arItem = $i->all();

            $total_price = 0;
            foreach ($arItem as $item){
                $total_price = $total_price + $item['quantity']*$item['price']; //TODO remove to add function
            }
            $sum[$k]['summ'] = $total_price;
        }
        return view('cart.index')
            ->with('items', $items_grouped)
            ->with('sum', $sum);
    }

    public function showSmall(){
        return view('cart.small', ['count' => $this->count()]);
    }

    public function getQuantity(){
        return json_encode(['quantity' => $this->count()]);
    }

    public function count(){
        return $this->getItems()->count();
    }

    public function getSumm(){
        return $this->getItems()->summ('price');
    }


    //all
    //delete
    //clear
    //getsumm
    //updatecount

}
