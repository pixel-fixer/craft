<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

class PasswordController extends Controller
{
	use ResetsPasswords;

	protected $redirectTo = '/';

	public function __construct()
	{
		$this->middleware('guest');
	}

	public function getEmail(){
		return view('auth.password')
			->with('title', 'Восстановление пароля');
	}

	public function getReset($token){
		return view('auth.reset')
			->with('title', 'Смена пароля')
			->with('token', $token);
	}
}
