<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    // После успешной авторизации
	protected $redirectPath = '/';

	protected $loginPath = '/login';

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;


	public function __construct()
	{
		$this->middleware('guest', ['except' => 'getLogout']);
	}

	public function username()
	{
		return 'email';
	}

    public function getLogin(){
		return view('auth.login')
			->with('title', 'Авторизация');
    }

    public function getRegister(){
    	return view('auth.register')
		->with('title', 'Регистрация');
    }

	protected function validator(array $data)
	{
		return Validator::make($data, [
			'username' => 'required|max:100',
			'email' => 'required|email|max:100|unique:users',
			'password' => 'required|confirmed|min:3',
		]);
	}

    protected function create(array $data)
    {
        return User::create([
            'username' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
}
