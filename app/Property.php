<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $table = 'property';


    public function values()
    {
        return $this->hasMany('App\PropertyEnum', 'property_id', 'id');
    }
}