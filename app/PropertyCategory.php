<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyCategory extends Model
{
    protected $table = 'property_category';

    public function properties()
    {
        return $this->hasOne('App\Property', 'id', 'property_id');
    }


}