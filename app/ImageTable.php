<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageTable extends Model
{
    protected $table = 'images';

    public function delete(){

        # delete image in folder
        $imageSmall = public_path() . "/img/products/small_". $this->patch;
        $imageBig = "/img/products/". $this->patch;

        if (file_exists($imageSmall))  unlink($imageSmall);
        if (file_exists($imageBig))  unlink($imageBig);

        # delete row in database
        parent::delete();
    }

}