<div data-menu='1' class="row catalog__menu__sub">
    <div class="col s12 m4">
        <div class="heading">
            Разделы
        </div>
        <div class="links">
            <a href="#">Украшения <span>{{ number_format(3910) }}</span></a>
            <a href="#">Украшения <span>{{ number_format(3910) }}</span></a>
            <a href="#">Мужская одежда и обувь <span>{{ number_format(3910) }}</span></a>
            <a href="#">Украшения <span>{{ number_format(3910) }}</span></a>
            <a href="#">Украшения <span>{{ number_format(3910) }}</span></a>
            <a href="#">Украшения <span>{{ number_format(3910) }}</span></a>
            <a href="#">Украшения <span>{{ number_format(3910) }}</span></a>
            <a href="#">Украшения <span>{{ number_format(3910) }}</span></a>
            <a href="#">Украшения <span>{{ number_format(3910) }}</span></a>
            <a href="#">Украшения <span>{{ number_format(3910) }}</span></a>
            <a href="#">Мужская одежда и обувь <span>{{ number_format(3910) }}</span></a>
        </div>
        <a href="#" class="handly_button arrow">
            В каталог
        </a>
    </div>
    <div class="col s12 m4">
        <div class="heading">
            Лучшие мастера
        </div>
        <div class="masters">
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
        </div>
        <a href="#" class="handly_button arrow">
            Все мастера
        </a>
    </div>
    <div class="col s12 m4">
        <div class="heading">
            Новые мастера
        </div>
        <div class="masters">
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
        </div>
        <a href="#" class="handly_button arrow">
            Все мастера
        </a>
    </div>
</div>
<div data-menu='2' class="row catalog__menu__sub">
    <div class="col s12 m4">
        <div class="heading">
            Разделы 2
        </div>
        <div class="links">
            <a href="#">Украшения <span>{{ number_format(3910) }}</span></a>
            <a href="#">Украшения <span>{{ number_format(3910) }}</span></a>
            <a href="#">Украшения <span>{{ number_format(3910) }}</span></a>
            <a href="#">Украшения <span>{{ number_format(3910) }}</span></a>
            <a href="#">Украшения <span>{{ number_format(3910) }}</span></a>
            <a href="#">Украшения <span>{{ number_format(3910) }}</span></a>
        </div>
        <a href="#" class="handly_button arrow">
            В каталог
        </a>
    </div>
    <div class="col s12 m4">
        <div class="heading">
            Лучшие мастера
        </div>
        <div class="masters">
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
        </div>
        <a href="#" class="handly_button arrow">
            Все мастера
        </a>
    </div>
    <div class="col s12 m4">
        <div class="heading">
            Новые мастера
        </div>
        <div class="masters">
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
        </div>
        <a href="#" class="handly_button arrow">
            Все мастера
        </a>
    </div>
</div>
<div data-menu='3' class="row catalog__menu__sub">
    <div class="col s12 m4">
        <div class="heading">
            Разделы 3
        </div>
        <div class="links">
            <a href="#">Украшения <span>{{ number_format(3910) }}</span></a>
            <a href="#">Украшения <span>{{ number_format(3910) }}</span></a>
            <a href="#">Мужская одежда и обувь <span>{{ number_format(3910) }}</span></a>
            <a href="#">Украшения <span>{{ number_format(3910) }}</span></a>
            <a href="#">Украшения <span>{{ number_format(3910) }}</span></a>
            <a href="#">Украшения <span>{{ number_format(3910) }}</span></a>
            <a href="#">Украшения <span>{{ number_format(3910) }}</span></a>
            <a href="#">Украшения <span>{{ number_format(3910) }}</span></a>
        </div>
        <a href="#" class="handly_button arrow">
            В каталог
        </a>
    </div>
    <div class="col s12 m4">
        <div class="heading">
            Лучшие мастера
        </div>
        <div class="masters">
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
        </div>
        <a href="#" class="handly_button arrow">
            Все мастера
        </a>
    </div>
    <div class="col s12 m4">
        <div class="heading">
            Новые мастера
        </div>
        <div class="masters">
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
        </div>
        <a href="#" class="handly_button arrow">
            Все мастера
        </a>
    </div>
</div>
<div data-menu='4' class="row catalog__menu__sub">
    <div class="col s12 m4">
        <div class="heading">
            Разделы 4
        </div>
        <div class="links">
            <a href="#">Украшения <span>{{ number_format(39210) }}</span></a>
            <a href="#">Украшения <span>{{ number_format(39210) }}</span></a>
            <a href="#">Мужская одежда и обувь <span>{{ number_format(3910) }}</span></a>
            <a href="#">Украшения <span>{{ number_format(39110) }}</span></a>
            <a href="#">Украшения <span>{{ number_format(39120) }}</span></a>
            <a href="#">Украшения <span>{{ number_format(39210) }}</span></a>
            <a href="#">Украшения <span>{{ number_format(32910) }}</span></a>
            <a href="#">Украшения <span>{{ number_format(23910) }}</span></a>
            <a href="#">Украшения <span>{{ number_format(13910) }}</span></a>
            <a href="#">Украшения <span>{{ number_format(13910) }}</span></a>
        </div>
        <a href="#" class="handly_button arrow">
            В каталог
        </a>
    </div>
    <div class="col s12 m4">
        <div class="heading">
            Лучшие мастера
        </div>
        <div class="masters">
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
        </div>
        <a href="#" class="handly_button arrow">
            Все мастера
        </a>
    </div>
    <div class="col s12 m4">
        <div class="heading">
            Новые мастера
        </div>
        <div class="masters">
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
            <a href="#">
                <img src="/img/menu-master-demo.png" alt="Woodhouse decor"/>
                <span>Woodhouse decor</span>
                <span>Санкт-Петербург</span>
            </a>
        </div>
        <a href="#" class="handly_button arrow">
            Все мастера
        </a>
    </div>
</div>
