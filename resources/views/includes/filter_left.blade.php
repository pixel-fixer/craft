<h2>Фильтр</h2>
{!! Form::open(['method'=>'get']) !!}
<div class="o-form-element">
	<div class="c-input-group c-input-group--stacked">
		<div class="o-field">
			{!! Form::text('price', '', ['class' => 'input-field', 'placeholder' => 'Цена изделия']) !!}
		</div>
		@if($categoryFilter)
			@foreach($categoryFilter as $filter)
				<div class="o-field" style="margin-top: 20px;">
					{!! $filter['NAME'] !!}
					@foreach($filter['VALUES'] as $f)
						@if($f->count_product)
							<p>
								{!! Form::checkbox('filter[]', $f->id, (isset($_GET['filter']) && in_array($f->id, $_GET['filter'])) ? 1 : 0 , ['class' => 'with-gap', 'id'=>'checkbox_'.$f->code]) !!}
								<label for="checkbox_{!! $f->code !!}">{!! $f->name !!} ({!! $f->count_product !!})</label>
							</p>
						@endif
					@endforeach
				</div>
			 @endforeach
		@endif
		<br/><br/>
	</div>
	<div class="o-form-element">
		{!! Form::submit('Найти', ['class' => 'btn orange']) !!}
	</div>
</div>
{!! Form::close() !!}