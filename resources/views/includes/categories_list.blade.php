<h2> {!! isset($category->name) ? $category->name : 'Кталог товаров'  !!}</h2>
<div class="small_seo_text">
        {!! isset($category->small_seo_text) ? $category->small_seo_text : ''  !!}
</div>
<ul class="section_list row">
    @foreach($categories as $category_one)
        <li class="col s4">
            {!! link_to_route('catalog_category', $category_one->name . " (" . rand(0, 9). ")", ['category' => $category_one->code ]) !!}
        </li>
    @endforeach
</ul>
<hr/>

@include('catalog.product_list')

<div class="big_seo_text">
    {!! isset($category->big_seo_text) ? $category->big_seo_text : ''  !!}
</div>
