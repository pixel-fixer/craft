@extends('layouts.main')

@section('content')
    <div class="content row">
        <div class="col s3">
            @include('cabinet.menu')
        </div>
        <div class="u-center-block col s9 personal_right">

            <h1>Редактирование изделия "{!! $product->name !!}"</h1>
            {!! Form::open(['route' => 'personal_product_edit', 'files' => true]) !!}
            {!! Form::hidden('id', $product->id, ['class'=> 'jsLoadProperty']) !!}
            {!! csrf_field() !!}
            <div class="o-form-element">
                <div class="c-input-group c-input-group--stacked">
                    <div class="o-field">
                        {!! Form::text('name', $product->name ? $product->name : old('name'), ['class' => 'input-field', 'placeholder' => 'Название изделия']) !!}
                    </div>


                    <div class="o-field">
                        <p>
                            {!! Form::hidden('active', 0) !!}

                            {!! Form::checkbox('active', '1', $product->active ? $product->active : old('active'), ['class' => 'with-gap', 'id'=>'item_active']) !!}
                            <label for="item_active">Активность</label>
                        </p>
                    </div>
                    <div class="o-field">
                        {!! Form::textarea('description', $product->description ? $product->description : old('description'), ['class' => 'materialize-textarea', 'placeholder' => 'Описание изделия']) !!}
                    </div>
                    <div class="o-field">
                        {!! Form::select('category_id', $category, $product->category_id ? $product->category_id : old('category_id'), ['placeholder' => 'Категория изделия']) !!}
                    </div>
                    <div id="LoadProperty" class="o-field">

                    </div>
                    <div class="o-field">
                        {!! Form::text('price', $product->price ? $product->price : old('price'), ['class' => 'input-field', 'placeholder' => 'Цена изделия']) !!}
                    </div>
                    @if (isset($product->images[0]))
                        <div class="row img-contain">
                            @foreach($product->images as $image)
                                <div class="col s2 edit_form__image">
                                    <a hre="/img/products/{!! $image->patch !!}" data-fancybox="gallery">
                                        <img src="/img/products/{!! $image->patch !!}" />
                                    </a>
                                        {!! Form::checkbox('del_image[]', $image->id, false, ['class' => 'with-gap', 'id'=>'del_image_'.$image->id]) !!}
                                        <label for="del_image_{!! $image->id !!}">Удалить</label>
                                </div>
                            @endforeach
                        </div>
                    @endif
                    <div class="row">
                        @for($i = 1; $i <= config('const.COUNT_FIELD_IMG_IN_FORM') ; $i++)
                            <div class="o-field col s4">
                                <div class="file-field input-field">
                                    <div class="btn orange">
                                        <span >Фотографии</span>
                                        {!! Form::file('image[]', false, ['class' => '']) !!}
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                </div>
                            </div>
                        @endfor
                    </div>
                </div>
                <div class="o-form-element">
                    {!! Form::submit('Сохранить', ['class' => 'btn orange', 'name' => 'save']) !!}
                    {!! Form::submit('Применить', ['class' => 'btn orange', 'name' => 'apply']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop