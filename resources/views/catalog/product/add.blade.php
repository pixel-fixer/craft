@extends('layouts.main')

@section('content')
    <div class="content row">
        <div class="col s3">
            @include('cabinet.menu')
        </div>
        <div class="u-center-block col s9 personal_right">

            <h1>Добавление нового изделия</h1>
            {!! Form::open(['route' => 'personal_product_add', 'files' => true]) !!}
            {!! csrf_field() !!}
            <div class="o-form-element">
                <div class="c-input-group c-input-group--stacked">
                    <div class="o-field">
                        {!! Form::text('name', old('name'), ['class' => 'input-field', 'placeholder' => 'Название изделия']) !!}
                    </div>
                    <div class="o-field">
                        <p>
                            {!! Form::checkbox('active', '1', old('active'), ['class' => 'with-gap', 'id'=>'item_active']) !!}
                            <label for="item_active">Активность</label>
                        </p>
                    </div>
                    <div class="o-field">
                        {!! Form::textarea('description',  old('description'), ['class' => 'materialize-textarea', 'placeholder' => 'Описание изделия']) !!}
                    </div>
                    <div class="o-field">
                        {!! Form::select('category_id', $category, old('category_id'), ['placeholder' => 'Категория изделия']) !!}
                    </div>
                    <div id="LoadProperty" class="o-field">

                    </div>
                    <div class="o-field">
                        {!! Form::text('price', old('price'), ['class' => 'input-field', 'placeholder' => 'Цена изделия']) !!}
                    </div>
                    <div class="row">
                        @for($i = 1; $i <= config('const.COUNT_FIELD_IMG_IN_FORM') ; $i++)
                            <div class="o-field col s4">
                                <div class="file-field input-field">
                                    <div class="btn orange">
                                        <span>Фотографии</span>
                                        {!! Form::file('image[]', false, ['class' => '']) !!}
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                </div>
                            </div>
                        @endfor
                    </div>
                </div>
                <div class="o-form-element">
                    {!! Form::submit('Сохранить', ['class' => 'btn orange']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop