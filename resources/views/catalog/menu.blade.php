<ul class="top_menu_ul">
    @foreach($menu as $category)
        <li  @if($category->level > 1) class="subsection" @endif>
            {!! link_to_route('catalog_category', $category->name, ['category' => $category->code ]) !!}
        </li>
    @endforeach
</ul>