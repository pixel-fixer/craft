@if($category)
    @foreach($category->properties as $CategoryProperty)
        <div style="margin-top: 20px">
            {!! $CategoryProperty->properties->name !!}
        </div>
        @if ($CategoryProperty->properties->type == 'list')
            <div class="">
                @foreach($CategoryProperty->properties->values as $value)
                    <p>
                        {!! Form::checkbox('property['.$CategoryProperty->properties->id.'][]', $value->id, isset($properties[$CategoryProperty->properties->id][$value->id]), ['class' => 'with-gap', 'id'=>'property_'.$value->id]) !!}
                        <label for="property_{!! $value->id !!}">{!! $value->name !!}</label>
                    </p>
                @endforeach
            </div>
        @elseif ($CategoryProperty->properties->type == 'string')
            {!! Form::text('property['.$CategoryProperty->properties->id.'][]', isset($properties[$CategoryProperty->properties->id][1]) ? $properties[$CategoryProperty->properties->id][1] : '', ['class' => 'input-field', 'placeholder' => $CategoryProperty->properties->name]) !!}
        @elseif ($CategoryProperty->properties->type == 'number')
            {!! Form::text('property['.$CategoryProperty->properties->id.'][]', isset($properties[$CategoryProperty->properties->id][1]) ? $properties[$CategoryProperty->properties->id][1] : '', ['class' => 'input-field', 'placeholder' => $CategoryProperty->properties->name]) !!}
        @endif
    @endforeach
@endif