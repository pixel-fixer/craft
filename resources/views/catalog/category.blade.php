@extends('layouts.main')

@section('content')
	<div class="content">
		<div class="row">
			<div class="col s3">
				@include('includes.filter_left')
			</div>
			<div class="col s9">
				@include('includes.categories_list')
			</div>
		</div>
	</div>
@stop