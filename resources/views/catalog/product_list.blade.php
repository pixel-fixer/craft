@if (isset($products) && !$products->isEmpty())
    <h2>Список товаров</h2>
    <div class="row">
        @foreach($products as $product)
            <div class="col s3 index_category one_product_list">
                <div class="one_product__image">
                    @if (!empty($product->images[0]))
                        <img src="/img/products/small_{!! $product->images[0]->patch !!}" class="index_category__img" />
                    @else
                        <img src="{!! config('const.NO_IMAGE') !!}" class="index_category__img" />
                    @endif
                </div>
                <div class="one_product__name">
                    {!! link_to_route('catalog_product', $product->name, ['category' => $product->code ]) !!}
                </div>
                @if (!empty($product->price))
                    <div class="one_product__price">

                        <b>{!! $product->price !!} Руб. </b>
                        <a href="javascript: void(0)"
                           class="{{ $product->inCart() ? 'in-cart' : '' }}"
                           onclick="addToCart({{$product->id}}, this); return false;">
                            <i class="material-icons small v-middle">shopping_cart</i>
                        </a>
                    </div>
                @endif
            </div>
        @endforeach
    </div>
    <div class="center-align">
        {!! $products->render() !!}
    </div>
@else
    <div class="center-align">
        Товаров в данном разделе не найдено
    </div>
@endif