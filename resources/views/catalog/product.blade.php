@extends('layouts.main')

@section('content')
	<div class="content cart_product">
		 <div class="row">
			<div class="col s6 cart_product__images">
				@if (isset($product->images[0]))
					<img src="/img/products/{!! $product->images[0]->patch !!}" />
				@else
					<img src="{!! config('const.NO_IMAGE') !!}" />
				@endif

			</div>
			<div class="col s6">
				<h1>{!! $product->name !!}</h1>

				<div class="cart_product__price">
					<b>{!! $product->price !!} Руб. </b>
					<a href="javascript: void(0)">
						<i class="material-icons small v-middle">shopping_cart</i>
					</a>
				</div>

				<div class="cart_product__description">
					{!! $product->description !!}
				</div>
				@if ($properties)
					<div class="cart_product__properties">
						<table>
							@foreach($properties as $property)
								<tr>
									<td style="vertical-align: top">
										<b>{!! $property['NAME'] !!}</b>
									</td>
									<td>
										@foreach($property['VALUE'] as $value)
											{!! $value !!} <br/>
										@endforeach
									</td>
								</tr>
							@endforeach
						</table>
					</div>
				@endif
				<div class="cart_product__description">
					{!! $product->description !!}
				</div>

				<div class="cart_product__master">
					<div>Мастер</div>
					{!! $master->first_name !!}
					{!! $master->last_name !!}
				</div>
			</div>

		</div>
	</div>
@stop