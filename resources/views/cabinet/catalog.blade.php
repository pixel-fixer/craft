@extends('layouts.main')

@section('content')
    <div class="content row">
        <div class="col s3">
            @include('cabinet.menu')
        </div>
        <div class="u-center-block col s9 personal_right">

        <div class="personal_catalog_menu left-align">
            <h2 class="left" style="margin-top: 0px;">Мои изделия</h2>
            <a href="{!! URL::route('personal_product_add_form') !!}" class="btn-floating right btn waves-effect waves-light orange">
                <i class="material-icons left">add</i>
            </a>
            <div class="clearfix"></div>
        </div>


        @if(isset($products))
            {!! Form::open() !!}
            {!! csrf_field() !!}
            <table class="personal_product_list">
                <thead>
                    <tr>
                        <th></th>
                        <th>ID</th>
                        <th></th>
                        <th>НАЗВАНИЕ</th>
                        <th>КАРТИНКА</th>
                        <th>ЦЕНА</th>
                        <th>КАГЕГОРИЯ</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td>

                            </td>
                            <td>{!! $product->id !!}</td>
                            <td>
                                <p>
                                    {!! Form::checkbox('item_active', $product->id, $product->active,
                                    ['class' => 'with-gap', 'disabled'=>'disabled', 'id' => 'item_active' . $product->id]) !!}
                                    <label for="item_active{!!  $product->id !!}"></label>
                                </p>

                            </td>
                            <td>{!! link_to_route('personal_product_edit_form', $product->name, ['id' => $product->id ]) !!}</td>
                            <td>
                                @if (!empty($product->images[0]))
                                    <img src="/img/products/{!! $product->images[0]->patch !!}" style="width: 50px" />
                                @else
                                    <img src="{!! config('const.NO_IMAGE') !!}" style="width: 50px" />
                                @endif
                            </td>
                            <td>{!! $product->price !!}</td>
                            <td>{!! $product->category_name !!}</td>
                            <td>
                                <a href="{!! URL::route('personal_product_edit_form', $product->id) !!}">
                                    <i class="material-icons">edit</i>
                                </a>
                            </td>
                            <td>
                                <a href="">
                                    <i class="material-icons">delete_forever</i>
                                </a>
                            </td>
                        </tr>
                     @endforeach
                </tbody>
            </table>
            {!! Form::close() !!}
            <div class="center-align">
                {!! $products->render() !!}
            </div>
        @endif
    </div>
    </div>
@stop