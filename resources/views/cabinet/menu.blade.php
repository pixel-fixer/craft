<div class="personal_left__menu">
    <a href="{!! URL::route('personal_page') !!}" class="waves-effect waves-light @if ($_SERVER['REQUEST_URI'] == '/personal')active @endif "><i class="material-icons left">person</i>Личные данные</a>
    <a href="{!! URL::route('personal_catalog') !!}" class="waves-effect waves-light @if (preg_match('#\/personal\/catalog.*#', $_SERVER['REQUEST_URI'])) active @endif"><i class="material-icons left">face</i>Мои изделия</a>
    <a href="/cabinet/order" class="waves-effect waves-light "><i class="material-icons left">featured_play_list</i>Заказы</a>
    <a href="/cabinet/im" class="waves-effect waves-light "><i class="material-icons left">message</i>Сообщения</a>
    <a href="/cabinet/settings" class="waves-effect waves-light "><i class="material-icons left">build</i>Настройки</a>
</div>