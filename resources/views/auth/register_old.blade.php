@extends('layouts.main')

@section('content')
	<div class="u-center-block" style="height: 380px;">
		<div class="u-center-block__content">
			<h1 class="c-heading">
				Регистрация
			</h1>
			{!! Form::open(['route' => 'register']) !!}
			{!! csrf_field() !!}
			<div class="o-form-element">
				<div class="c-input-group c-input-group--stacked">
					<div class="o-field">
						{!! Form::text('first_name', Input::old('first_name'), ['class' => 'c-field', 'placeholder' => 'Ваше имя']) !!}
					</div>
					<div class="o-field">
						{!! Form::text('last_name', Input::old('last_name'), ['class' => 'c-field', 'placeholder' => 'Ваша фамилия']) !!}
					</div>
					<div class="o-field">
						{!! Form::email('email', Input::old('email'), ['class' => 'c-field', 'placeholder' => 'Email адрес']) !!}
					</div>
					<div class="o-field">
						{!! Form::password('password', ['placeholder' => 'Пароль', 'class' => 'c-field']) !!}
					</div>
					<div class="o-field">
						{!! Form::password('password_confirmation', ['placeholder' => 'Повторите', 'class' => 'c-field']) !!}
					</div>
				</div>
			</div>
			<div class="o-form-element">
				{!! Form::submit('Регистрация', ['class' => 'c-button c-button--brand c-button--block']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
@stop