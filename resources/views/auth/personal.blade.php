@extends('layouts.main')

@section('content')

	<div class="content row">
		<div class="col s3">
			@include('cabinet.menu')
		</div>
		<div class="u-center-block col s9 personal_right">
			<div class="u-center-block__content">
				<h1 class="c-heading">
					Личный кабинет
				</h1>
				{!! Form::open(['route' => 'personal_update', 'files' => true]) !!}
					{!! csrf_field() !!}
					<div class="o-form-element">
						<div class="c-input-group c-input-group--stacked">
							<div class="o-field">
								{!! Form::text('first_name', Auth::user()->first_name, ['class' => 'c-field', 'placeholder' => 'Ваше имя']) !!}
							</div>
							<div class="o-field">
								{!! Form::text('last_name', Auth::user()->last_name, ['class' => 'c-field', 'placeholder' => 'Ваша фамилия']) !!}
							</div>
							<div class="o-field">
								{!! Form::email('email', Auth::user()->email, ['class' => 'c-field', 'placeholder' => 'Email адрес']) !!}
							</div>
							<div class="o-field row">
								<div class="col s2 personal_photo">
									@if (!empty(Auth::user()->photo))
										<img src="/img/personal/small_{!! Auth::user()->photo !!}" />
									@else
										<img src="{!! config('const.NO_IMAGE') !!}" />
									@endif
								</div>

								<div class="file-field input-field col s10">
									<div class="btn orange">
										<span>Аватар</span>
										{!! Form::file('photo', false, ['class' => '']) !!}
									</div>
									<div class="file-path-wrapper">
										<input class="file-path validate" type="text">
									</div>
								</div>
							</div>
							<div class="o-field">
								{!! Form::textarea('about', Auth::user()->about, ['class' => 'materialize-textarea',  'placeholder' => 'О себе']) !!}
							</div>
							<div class="o-field">
								{!! Form::select('sex', ['men' => 'Мужской', 'women' => 'Женский'],Auth::user()->sex, ['placeholder' => 'Пол']) !!}
							</div>
							<div class="o-field">
								{!! Form::password('password', ['placeholder' => 'Новый пароль', 'class' => 'c-field']) !!}
							</div>
							<div class="o-field">
								{!! Form::password('repeat_password', ['placeholder' => 'Повторите', 'class' => 'c-field']) !!}
							</div>
						</div>
					</div>
					<div class="o-form-element">
						{!! Form::submit('Сохранить', ['class' => 'btn orange']) !!}
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@stop