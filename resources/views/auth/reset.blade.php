@extends('layouts.main')

@section('content')
	<div class="u-center-block" style="height: 340px;">
		<div class="u-center-block__content">
			<h1 class="c-heading">
				Смена пароля
			</h1>
			{!! Form::open(['route' => 'reset_password']) !!}
				{!! csrf_field() !!}
				{!! Form::hidden('token', $token)  !!}
				<div class="o-form-element">
					<div class="c-input-group c-input-group--stacked">
						<div class="o-field">
							{!! Form::email('email', Input::old('email'), ['class' => 'c-field', 'placeholder' => 'Email адрес']) !!}
						</div>
						<div class="o-field">
							{!! Form::password('password', ['placeholder' => 'Пароль', 'class' => 'c-field']) !!}
						</div>
						<div class="o-field">
							{!! Form::password('password_confirmation', ['placeholder' => 'Повторите пароль', 'class' => 'c-field']) !!}
						</div>
					</div>
				</div>
				<div class="o-form-element">
					{!! Form::submit('Войти', ['class' => 'c-button c-button--brand c-button--block']) !!}
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@stop