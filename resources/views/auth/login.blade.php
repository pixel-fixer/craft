@extends('layouts.main')

@section('content')
	<div class="content">
	<div class="u-center-block" style="height: 340px;">
		<div class="u-center-block__content">
			<h1 class="c-heading">
				Авторизация
			</h1>
			{!! Form::open(['route' => 'login']) !!}
				{!! csrf_field() !!}
				<div class="o-form-element">
					<div class="c-input-group c-input-group--stacked">
						<div class="o-field">
							{!! Form::email('email', Input::old('email'), ['class' => 'c-field', 'placeholder' => 'Email адрес']) !!}
						</div>
						<div class="o-field">
							{!! Form::password('password', ['placeholder' => 'Пароль', 'class' => 'c-field']) !!}
						</div>
					</div>
				</div>
				<fieldset class="o-fieldset c-list c-list--inline c-list--unstyled">
					<label class="o-form-element c-label c-list__item">
						{!! Form::checkbox('remember', Input::old('remember')) !!}
						Запомнить
					</label>
					<label class="o-form-element c-label c-list__item">
						{!! link_to_route('get_reset_token_page', 'Забыли пароль?', [], ['class' => 'u-color-brand-light']) !!}
					</label>
				</fieldset>
				<div class="o-form-element">
					{!! Form::submit('Войти', ['class' => 'btn orange']) !!}
				</div>
			{!! Form::close() !!}
		</div>
	</div>
	</div>
@stop