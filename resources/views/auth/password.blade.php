@extends('layouts.main')

@section('content')
	<div class="u-center-block" style="height: 340px;">
		<div class="u-center-block__content">
			<h1 class="c-heading">
				Восстановление пароля
			</h1>
			{!! Form::open(['route' => 'get_reset_token']) !!}
				{!! csrf_field() !!}
				<div class="o-form-element">
					<div class="c-input-group c-input-group--stacked">
						<div class="o-field">
							{!! Form::email('email', Input::old('email'), ['class' => 'c-field', 'placeholder' => 'Email адрес']) !!}
						</div>
					</div>
				</div>
				<div class="o-form-element">
					{!! Form::submit('Сбросить', ['class' => 'c-button c-button--brand c-button--block']) !!}
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@stop