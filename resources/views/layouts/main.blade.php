<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Handly.PRO &bull; {{ isset($title) ? $title : 'страница без заголовка' }}</title>
		<meta name="description" content="{{ isset($description) ? $description : 'description' }}">
		<meta name="keywords" content="{{ isset($keywords) ? $keywords : 'keywords' }}">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&amp;subset=latin-ext" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.min.css" />
		<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
        {!! Html::style('css/vendor.css') !!}
		{!! Html::style('css/materialize.css') !!}
		{!! Html::style('css/handly.css?ver='. filemtime(public_path() . '/css/handly.css')) !!}
		@yield('head_scripts')
	</head>
	<body class="page-{{ Route::currentRouteName() }}">
		<div class="overlay"></div>
		<div id="page_wrap">
			<header>
				<div class="header__top">
					<div class="container padding">
						<a class="logo" href="{{ URL::route('home') }}">
							<img src="/img/logo.png" alt='Handly'/>
						</a>
						<nav class="header__nav">
                            {!! link_to_route('catalog', 'О нас') !!}
                            {!! link_to_route('catalog', 'Мастерам') !!}
                            {!! link_to_route('catalog', 'Покупателям') !!}
                            {!! link_to_route('catalog', 'Блог') !!}
                            {!! link_to_route('catalog', 'Помощь') !!}
						</nav>
						<div class="header__nav__user">
							{!! link_to_route('catalog', 'Создать магазин', [], ['class' => 'main_link']) !!}
							@if (Auth::check())
								<a href="">personal</a>
							@else
								{!! link_to_route('login', 'Регистрация / Войти', [], ['class' => 'main_link']) !!}
							@endif
							<a class="fast_link" data-quantity="" href="{{ URL::route('view_cart') }}"></a>
							<a class="fast_link" id="cart" data-quantity="" href="{{ URL::route('view_cart') }}"></a>
						</div>
					</div>
				</div>
				<div class="header__second">
					<div class="container">
						<a class="burger" href="{{ URL::route('catalog') }}"></a>
						<div class="catalog__menu">
							<a class="main_link" data-formenu="1" href="{{ URL::route('catalog') }}">
								Одежда и аксессуары
							</a>
							<a class="main_link" data-formenu="2" href="{{ URL::route('catalog') }}">
								Украшения
							</a>
							<a class="main_link" data-formenu="3" href="{{ URL::route('catalog') }}">
								Товары для рукоделия
							</a>
							<a class="main_link" data-formenu="4" href="{{ URL::route('catalog') }}">
								Свадьба
							</a>
						</div>
						<div class="right">
							<form id="main__search">
								<input type="text" name="searchstr" placeholder="Что будем искать?" spellcheck="false" autocomplete="off"/>
								<input type="submit" value=""/>
							</form>
						</div>
					</div>
					@include('includes.header_overlay_menu')
				</div>
			</header>
            @if (count($errors) > 0)
                <div class="container">
                    <div class="center-align">
                        <div class="u-center-block__content u-center-block__content--horizontal">
                            <ul class="c-list">
                                @foreach ($errors->all() as $error)
                                    <li class="c-list__item">{!! $error !!}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            @endif
            <main>
                @if(isset($breadCrumbs))
                    <div class="container">
                        <ul>
                            @foreach($breadCrumbs as $key => $bc)
                                @if($key != (count($breadCrumbs) - 1))
                                    <li style="display: inline-block;">
                                        {{ link_to($bc['url'], $bc['anchor']) }}
                                    </li>
                                    <li style="display: inline-block;"> >  </li>
                                @else
                                    <li style="display: inline-block;">
                                        {{ $bc['anchor'] }}
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                @endif
                @yield('content')
            </main>
		</div>
		<footer>
            <div class="container row">
				<div class="col s12 m3 main_footer_links">
					<a href="{{ URL::route('catalog') }}">
						О нас
					</a>
					<a href="{{ URL::route('catalog') }}">
						Помощь
					</a>
					<a href="{{ URL::route('catalog') }}">
						Блог
					</a>
					<a href="{{ URL::route('catalog') }}">
						Мастерам
					</a>
					<a href="{{ URL::route('catalog') }}">
						Покупателям
					</a>
					<a href="{{ URL::route('catalog') }}">
						Служба поддержки
					</a>
				</div>
				<div class="col s12 m3 social">
					<div>
						Хотите, чтобы ваши работы<br/>
						увидели все? Пишите в наши<br/>
						группы в соцсетях. Опубликуем<br/>
						ваши изделия Вконтакте<br/>
						и Инстаграме бесплатно!
					</div>
					<div class="links">
						<a target="_blank" href="#" class="fb">
							&nbsp;
						</a>
						<a target="_blank" href="https://vk.com/handlyportal" class="vk">
							&nbsp;
						</a>
						<a target="_blank" href="https://www.instagram.com/handlyportal/" class="ig">
							&nbsp;
						</a>
					</div>
					<a class="handly_button" href="javascript: void(0);">
						Сообщить об ошибке
					</a>
				</div>
				<div class="col s12 m3 vk">
					<div class="heading">
						Vkontakte
					</div>
					<script type="text/javascript" src="//vk.com/js/api/openapi.js?146"></script>
					<div id="vk_groups"></div>
				</div>
				<div class="col s12 m3 ig">
					<div class="heading">
						Instagram #HANDLY.PRO
					</div>
				</div>
            </div>
		</footer>
		{!! Html::script('js/jquery.js?' . filemtime(public_path() . '/js/jquery.js'), ['type' => 'text/javascript']) !!}
		{!! Html::script('js/materialize.min.js?' . filemtime(public_path() . '/js/materialize.min.js'), ['type' => 'text/javascript']) !!}
		{!! Html::script('js/main.js?' . filemtime(public_path() . '/js/main.js'), ['type' => 'text/javascript']) !!}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.min.js"></script>
		<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
        @yield('footer_scripts')
	</body>
</html>
