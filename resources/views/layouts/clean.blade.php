<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Handly.PRO &bull; {{ isset($title) ? $title : 'страница без заголовка' }}</title>
    <meta name="description" content="{{ isset($description) ? $description : 'description' }}">
    <meta name="keywords" content="{{ isset($keywords) ? $keywords : 'keywords' }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&amp;subset=latin-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.min.css" />
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
    {!! Html::style('css/vendor.css') !!}
    {!! Html::style('css/materialize.css') !!}
    {!! Html::style('css/main.css?ver='. filemtime(public_path() . '/css/main.css')) !!}
    @yield('head_scripts')
</head>
<body class="page-{{ Route::currentRouteName() }}">
<div id="page_wrap">
    <header>

    </header>
    <main>
        @yield('content')
    </main>
</div>
<footer>

</footer>
{!! Html::script('js/jquery.js?' . filemtime(public_path() . '/js/jquery.js'), ['type' => 'text/javascript']) !!}
{!! Html::script('js/materialize.min.js?' . filemtime(public_path() . '/js/materialize.min.js'), ['type' => 'text/javascript']) !!}
{!! Html::script('js/main.js?' . filemtime(public_path() . '/js/main.js'), ['type' => 'text/javascript']) !!}
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
@yield('footer_scripts')
</body>
</html>
