@extends('layouts.main')

@section('footer_scripts')
    <script>
    $('.main_best_masters_slider').slick({
        dots: false,
        infinite: false,
        slidesToShow: 4,
        slidesToScroll: 1,
        prevArrow: '<button type="button" class="slick-prev"></button>',
        nextArrow: '<button type="button" class="slick-next"></button>'
    });
    </script>
@stop

@section('content')
    <ul id="main_slider">
        <li class="slide" style="background-image: url('/img/cube_slide.png');">
            <div class="container row">
                <div class="col s12 m6">
                    <div class="main_text">
                        Уникальная<br/>
                        handmade<br/>
                        площадка
                    </div>
                    <div class="secondary_text">
                        Площадка, где мастера и покупатели найдут друг друга.
                    </div>
                </div>
            </div>
        </li>
    </ul>
    @if ( count($categories) )
        <div class="white_wrap">
            <div class="container padding">
                <div class="main_categories_list">
                    <div class="main_category_item text_item">
                        <span class="heading">
                            Популярные разделы
                        </span>
                        <span class="description">
                            Пишите в наши группы в соцсетях.<br/>
                            Опубликуем ваши изделия Вконтакте
                        </span>
                    </div>
                    @foreach( $categories as $category )
                        <a class="main_category_item" href="{!! URL::route('catalog_category', ['category' => $category->code ]) !!}" style="background-image: url('/img/category/{{ $category->id }}.jpg');">
                            <span class="main_category_name">
                                {{ $category->name }}
                            </span>
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    @endif
    @if ( count($products) )
        <div class="white_wrap">
            <div class="container padding">
                <div class="row main_products_list">
                    <div class="col s6 m3">
                        <div class="main_product_item text_item">
                            <span class="heading">
                                Лучшие<br/>
                                товары
                            </span>
                            <span class="description">
                                Пишите в наши группы в соцсетях.<br/>
                                Опубликуем ваши изделия Вконтакте
                            </span>
                            <a href="{{ URL::route('catalog') }}" class="handly_button arrow">
                                В каталог
                            </a>
                        </div>
                    </div>
                    @foreach($products as $product)
                        <div class="col s6 m3">
                            <div class="main_product_item">
                                <a class="main_product_image" href="{{ URL::route('catalog_product', ['category' => $product->code ]) }}" style="background-image: url(' {{ !empty($product->images[0]) ? ('/img/products/small_' . $product->images[0]->patch) : config('const.NO_IMAGE') }} ');">
                                    &nbsp;
                                </a>
                                <div class="main_product_item_footer">
                                    <a class="main_product_name" href="{{ URL::route('catalog_product', ['category' => $product->code ]) }}">
                                        {{ $product->name }}
                                    </a>
                                    <a class="shop_and_city" href="javascript: void(0);">
                                        Griffin Goods <span>/</span> Москва
                                    </a>
                                    <div class="main_product_item_controls">
                                        <span class="price">
                                            {{ number_format($product->price) }} <span class="fa fa-rub"></span>
                                        </span>
                                        <a href="javascript: void(0);" class="favorite">
                                        </a>
                                        <a href="javascript: void(0);" class="add_to_cart{{ $product->inCart() ? ' in-cart' : '' }}" onclick="addToCart({{$product->id}}, this); return false;">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif
    <div class="white_wrap home_sale_with_us">
        <div class="form">
            <div class="container row">
                <div class="col s12 m7">
                    <div class="heading">
                        Продавай вместе с Хендли
                    </div>
                    <div class="row">
                        <div class="col s6 description">
                            Откройте свой магазин сегодня<br/>
                            и зарабатывайте на любимом хобби.
                        </div>
                        <div class="col s6">
                            <a href="{{ URL::route('catalog') }}" class="handly_button">
                                Создайте свой магазин
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="white_wrap main_best_masters">
        <div class="container row">
            <div class="col s12 m4">
                <div class="heading">
                    Лучшие мастера
                </div>
                <div class="description">
                    На нашей площадке вы сможете найти лучших<br/>
                    мастеров. Посмотрифте каталог их работ,<br/>
                    свяжитесь с ними напрямую или станьте<br/>
                    одним из мастеров
                </div>
                <a href="{{ URL::route('catalog') }}" class="handly_button">
                    Все мастера
                </a>
            </div>
            <div class="col s12 m8">
                <ul class="main_best_masters_slider">
                    <li>
                        <div class="slide_inset_wrap">
                            <div class="image"><img src="/img/master_demo.png" alt="DecorLAB"/></div>
                            <span class="name">DecorLAB</span>
                            <span class="city">Санкт-Петербург</span>
                        </div>
                    </li>
                    <li>
                        <div class="slide_inset_wrap">
                            <div class="image"><img src="/img/master_demo.png" alt="DecorLAB"/></div>
                            <span class="name">DecorLAB</span>
                            <span class="city">Санкт-Петербург</span>
                        </div>
                    </li>
                    <li>
                        <div class="slide_inset_wrap">
                            <div class="image"><img src="/img/master_demo.png" alt="DecorLAB"/></div>
                            <span class="name">DecorLAB</span>
                            <span class="city">Санкт-Петербург</span>
                        </div>
                    </li>
                    <li>
                        <div class="slide_inset_wrap">
                            <div class="image"><img src="/img/master_demo.png" alt="DecorLAB"/></div>
                            <span class="name">DecorLAB</span>
                            <span class="city">Санкт-Петербург</span>
                        </div>
                    </li>
                    <li>
                        <div class="slide_inset_wrap">
                            <div class="image"><img src="/img/master_demo.png" alt="DecorLAB"/></div>
                            <span class="name">DecorLAB</span>
                            <span class="city">Санкт-Петербург</span>
                        </div>
                    </li>
                    <li>
                        <div class="slide_inset_wrap">
                            <div class="image"><img src="/img/master_demo.png" alt="DecorLAB"/></div>
                            <span class="name">DecorLAB</span>
                            <span class="city">Санкт-Петербург</span>
                        </div>
                    </li>
                    <li>
                        <div class="slide_inset_wrap">
                            <div class="image"><img src="/img/master_demo.png" alt="DecorLAB"/></div>
                            <span class="name">DecorLAB</span>
                            <span class="city">Санкт-Петербург</span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="main_bottom_form">
        <div class="container row">
            <div class="col s12 m6">
                <div class="heading">
                    Каким вы хотите видеть<br/>
                    новый портал хендмейда?
                </div>
            </div>
            <div class="col s12 m6" style="clear: both;">
                <div>
                    Напишите в форме ниже, что вам не нравится на других сайтах<br/>
                    хендмейда? Что бы вы хотели видеть на нашем портале HANDLY?
                </div>
                <div class="star-list-heading">
                    Пример рекомендации:
                </div>
                <ul class="star-list">
                    <li>
                        Я хочу, чтобы портал было удобно посещать с телефона
                    </li>
                    <li>
                        Хочу заливать как можно больше своих работ на сайт
                    </li>
                    <li>
                        Мне не нравится слишком строгая модерация
                    </li>
                </ul>
            </div>
            <div class="col s12 m6">
                <form>
                    <textarea></textarea>
                    <input type="checkbox" class="handly-checkbox" name="agree" id="agree"/>
                    <label for="agree">
                        Согласен на обработку моих персональных данных
                    </label>
                    <br/>
                    <input type="submit" value="Отправить" class="handly_button"/>
                </form>
            </div>
        </div>
    </div>
@stop
