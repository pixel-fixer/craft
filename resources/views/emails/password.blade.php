@extends('layouts.email')

@section('content')
	Для сброса пароля перейдите по ссылке: {!! link_to_route('reset_password_page', $token, ['token' => $token]) !!}
@stop