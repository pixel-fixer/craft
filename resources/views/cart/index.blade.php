@extends('layouts.main')

@section('content')
    <div class="content">
        <h1>Корзина заказов</h1>
        <div>
            @if(count($items))
                @foreach( $items as $user_id => $user_items)
                    <div class="cart__list">
                        Продавец: <a href="/{{ '@'.$sum[$user_id]['id']}}">{{ $sum[$user_id]['name'] }}</a>
                        <table>
                            <thead>
                            <tr>
                                <th></th>
                                <th>Название</th>
                                <th>Цена</th>
                                <th>Количество</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($user_items as $item)
                                 <tr>
                                    <td class="cart__list__img">
                                      @if($item['images'])
                                            <img src="/img/products/{{$item['images'][0]['patch']}}" alt="">
                                      @else
                                            <img src="{{config('const.NO_IMAGE')}}" alt="">
                                      @endif
                                    </td>
                                    <td>
                                        <b>{{$item['name']}}</b><br>
                                        {{ $item['description'] }}
                                    </td>
                                    <td>{{ $item['price'] }}</td>
                                    <td>
                                         <div class="cart__list__quantity">
                                             <a href="" class="cart__list__quantity__down">
                                                 <i class="fa fa-angle-down" aria-hidden="true"></i>
                                             </a>
                                             <input type="text" data-token='{{csrf_token()}}' data-id='{{ $item['id']  }}' name="quantity" value="{{ $item['quantity'] }}">
                                             <a href="" class="cart__list__quantity__up">
                                                 <i class="fa fa-angle-up" aria-hidden="true"></i>
                                             </a>
                                         </div>
                                     </td>
                                     <td>
                                         <a href="/cart/delete/{{ $item['id'] }}">
                                             <i class="fa fa-times" aria-hidden="true"></i>
                                         </a>
                                     </td>
                                 </tr>
                            @endforeach
                            <tr>
                                <td colspan="4">Сумма заказ: {{ $sum[$user_id]['summ'] }}</td>
                            </tr>
                            </tbody>
                        </table>
                        <a href="/order/make/" class="btn orange">Оформить заказ</a>
                    </div>

                @endforeach
                <br>
                <br>

            @else
                <div>Не найдено ни одного заказ</div>
                <a href="/catalog/" class="btn orange">Продолжить покупки</a>
            @endif
        </div>
    </div>
@endsection