<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>{!! isset($title) ? $title : 'untitled' !!} &bull; Handly.PRO</title>
		<meta name="description" content="Handly website.">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
		{!! Html::style('css/vendor.css') !!}
		{!! Html::style('css/blaze.min.css') !!}
		{!! Html::style('css/blaze.colors.min.css') !!}
		{!! Html::style('css/main.css?ver=1') !!}
		@yield('head_scripts')
	</head>
	<body>
		<header>
			<nav class="c-nav c-nav--inline c-nav--high">
				{!! link_to_route('home', 'Главная', [], ['class' => 'c-nav__item']) !!}
				@if(Auth::check())
					{!! link_to_route('logout', 'Выход',[] , ['class' => 'c-nav__item c-nav__item--right']) !!}
					<span class="c-nav__item c-nav__item--right">
						{!! link_to_route('personal_page', 'Личный кабинет',[] , ['class' => 'c-nav__item c-nav__item--right']) !!}
					</span>
				@else
					{!! link_to_route('register_page', 'Регистрация',[] , ['class' => 'c-nav__item c-nav__item--right']) !!}
					{!! link_to_route('login_page', 'Вход',[], ['class' => 'c-nav__item c-nav__item--right']) !!}
				@endif
			</nav>
		</header>
		<div class="content">
			@if (count($errors) > 0)
				<div class="u-center-block">
					<div class="u-center-block__content u-center-block__content--horizontal">
						<ul class="c-list">
							@foreach ($errors->all() as $error)
								<li class="c-list__item">{!! $error !!}</li>
							@endforeach
						</ul>
					</div>
				</div>
			@endif
			@if(Session::has('messages'))
				<div class="u-center-block">
					<div class="u-center-block__content u-center-block__content--horizontal">
						<ul class="c-list">
							@foreach (Session::get('messages') as $message)
								<li class="c-list__item">{!! $message !!}</li>
							@endforeach
						</ul>
					</div>
				</div>
			@endif
			<main>
				@yield('content')
			</main>
		</div>
		<footer class="u-centered">
			&copy; Handly
		</footer>
		{!! Html::script('js/vendor.js', ['type' => 'text/javascript']) !!}
		@yield('footer_scripts')
	</body>
</html>
