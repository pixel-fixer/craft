<?php

return [
    'required' => 'Поле :attribute обязательно для заполнения',
    'unique'   => ':attribute уже занят',
    'max'                  => [
        'numeric' => 'Поле :attribute должно быть не более, чем :max.',
    ],
    'min'                  => [
        'numeric' => ':attribute должно быть не меньше, чем :min.',
        'string' => ':attribute должно быть не меньше, чем :min символа',

    ],


    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],


    'attributes' => [
        'password' => 'пароль',
        'username' => 'логин'
    ]
];